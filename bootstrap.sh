PIO_BASE=~/.platformio
PIO_LIB_PATH=$PIO_BASE/packages/framework-arduinoavr/libraries/
PIO_VARIANTS_PATH=$PIO_BASE/packages/framework-arduinoavr/variants

cp -R ~/projects/ATmega328PB-Testing/hardware/atmega328pb/avr/variants/atmega328pb/ $PIO_VARIANTS_PATH
cp -R ~/projects/ATmega328PB-Testing/hardware/atmega328pb/avr/libraries/{SPI1,Wire1} $PIO_LIB_PATH
