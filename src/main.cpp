#include "Arduino.h"

#include "pins.h"
#include "RS485NodeProto.h"

RS485Controller rs485ctrl(Serial, RS485_RE, RS485_WE);

RS485NodeProto  rs485proto(rs485ctrl, 2);

String readstr;

void setup() {
  pinMode(DOOR_PIN, OUTPUT);

  pinMode(RS485_WE, OUTPUT);
  pinMode(READER_RS485_WE, OUTPUT);

  Serial.begin(9600);
  rs485ctrl.begin();
}


RS485NodeProto::RecvStatus rxstatus = RS485NodeProto::RECV_ERROR;

void loop() {
  RS485NodeProto::RecvStatus newRXstatus = rs485proto.receivePacket(0);

  // Print the status
	if (newRXstatus != rxstatus) {
		rxstatus = newRXstatus;

		switch(newRXstatus) {
		case RS485NodeProto::RECV_IDLE: 	 Serial.println("RX: IDLE"); break;
		case RS485NodeProto::RECV_BUSY:		 Serial.println("RX: BUSY"); break;
		case RS485NodeProto::RECV_DATA_READY:
      char msg[RS485_RX_BUFFERSIZE];
      memcpy(msg, rs485proto.inPacket.buffer, rs485proto.inPacket.length);
		case RS485NodeProto::RECV_ERROR:		 Serial.println("RX: ERROR"); break;
		case RS485NodeProto::RECV_TIMEOUT:		 Serial.println("RX: TIMEOUT"); break;
		}
	}

}
